package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Cancohorfec;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Abogado entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CancohorfecRepository extends JpaRepository<Cancohorfec, Long> {

}
