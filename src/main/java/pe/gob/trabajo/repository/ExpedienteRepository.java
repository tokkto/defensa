package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Expediente;
import org.springframework.stereotype.Repository;
import java.util.List;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Expediente entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExpedienteRepository extends JpaRepository<Expediente, Long> {

    @Query("select E from Expediente E where ('0' = ?3 or E.vNumexp = ?3) and  E.trabajador.pernatural.vNumdoc = ?2 and E.trabajador.pernatural.tipdocident.id = ?1 and E.nFlgactivo = 1 ")
    List<Expediente>findExpedienteNroDoc(Long tip_doc, String nro_doc, String nro_exp);

    @Query("select E from Expediente E " +
    "where  " +
    "E.id in ( select E1.id from Expediente E1 " +
             " where E1.pasegl.atencion.datlab.trabajador.pernatural.vNumdoc = ?2 and E1.pasegl.atencion.datlab.trabajador.pernatural.tipdocident.id = ?1 " +
             " and E1.pasegl.atencion.datlab.empleador.perjuridica.nFlgactivo = 1 and E1.nFlgactivo = 1 and E1.pasegl.nFlgactivo = 1 " +
             " and E1.pasegl.atencion.nFlgactivo = 1 and E1.pasegl.atencion.datlab.nFlgactivo = 1 ) or " +
    "E.id in ( select E2.id from Expediente E2 " +
             " where E2.pasegl.atencion.datlab.empleador.pernatural.vNumdoc = ?2 and E2.pasegl.atencion.datlab.empleador.pernatural.tipdocident.id = ?1 " +
             " and E2.pasegl.atencion.datlab.empleador.perjuridica.nFlgactivo = 1 and E2.nFlgactivo = 1 and E2.pasegl.nFlgactivo = 1 " +
             " and E2.pasegl.atencion.nFlgactivo = 1 and E2.pasegl.atencion.datlab.nFlgactivo = 1 ) or " +
    "E.id in ( select E3.id from Expediente E3 " +
             " where E3.pasegl.atencion.datlab.empleador.perjuridica.vNumdoc = ?2 and E3.pasegl.atencion.datlab.empleador.perjuridica.nFlgactivo = 1" +
             " and E3.nFlgactivo = 1 and E3.pasegl.nFlgactivo = 1 and E3.pasegl.atencion.nFlgactivo = 1 and E3.pasegl.atencion.datlab.nFlgactivo = 1 ) " )
    List<Expediente>ListarExpedienteNroDocParam(Long tip_doc, String nro_doc);

   /*@Query("select E from Expediente E " +
    "where   ")
    List<Expediente>ListarExpedienteResAudConcParam(Long tip_doc, String nro_doc);*/

}
