package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipproveid;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipproveid entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipproveidRepository extends JpaRepository<Tipproveid, Long> {

}
