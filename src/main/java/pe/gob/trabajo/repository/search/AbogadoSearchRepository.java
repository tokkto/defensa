package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Abogado;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Abogado entity.
 */
public interface AbogadoSearchRepository extends ElasticsearchRepository<Abogado, Long> {
}
