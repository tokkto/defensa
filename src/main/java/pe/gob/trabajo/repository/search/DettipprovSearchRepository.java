package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Dettipprov;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Dettipprov entity.
 */
public interface DettipprovSearchRepository extends ElasticsearchRepository<Dettipprov, Long> {
}
