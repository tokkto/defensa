package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Resulconci;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Resulconci entity.
 */
public interface ResulconciSearchRepository extends ElasticsearchRepository<Resulconci, Long> {
}
