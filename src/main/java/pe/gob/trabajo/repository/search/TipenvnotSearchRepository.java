package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipenvnot;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipenvnot entity.
 */
public interface TipenvnotSearchRepository extends ElasticsearchRepository<Tipenvnot, Long> {
}
