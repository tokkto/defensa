package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipresconc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipresconc entity.
 */
public interface TipresconcSearchRepository extends ElasticsearchRepository<Tipresconc, Long> {
}
