package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Multaconci;

import pe.gob.trabajo.repository.MultaconciRepository;
import pe.gob.trabajo.repository.search.MultaconciSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Multaconci.
 */
@RestController
@RequestMapping("/api")
public class MultaconciResource {

    private final Logger log = LoggerFactory.getLogger(MultaconciResource.class);

    private static final String ENTITY_NAME = "multaconci";

    private final MultaconciRepository multaconciRepository;

    private final MultaconciSearchRepository multaconciSearchRepository;

    public MultaconciResource(MultaconciRepository multaconciRepository, MultaconciSearchRepository multaconciSearchRepository) {
        this.multaconciRepository = multaconciRepository;
        this.multaconciSearchRepository = multaconciSearchRepository;
    }

    /**
     * POST  /multaconcis : Create a new multaconci.
     *
     * @param multaconci the multaconci to create
     * @return the ResponseEntity with status 201 (Created) and with body the new multaconci, or with status 400 (Bad Request) if the multaconci has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/multaconcis")
    @Timed
    public ResponseEntity<Multaconci> createMultaconci(@Valid @RequestBody Multaconci multaconci) throws URISyntaxException {
        log.debug("REST request to save Multaconci : {}", multaconci);
        if (multaconci.getId() != null) {
            throw new BadRequestAlertException("A new multaconci cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Multaconci result = multaconciRepository.save(multaconci);
        multaconciSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/multaconcis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /multaconcis : Updates an existing multaconci.
     *
     * @param multaconci the multaconci to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated multaconci,
     * or with status 400 (Bad Request) if the multaconci is not valid,
     * or with status 500 (Internal Server Error) if the multaconci couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/multaconcis")
    @Timed
    public ResponseEntity<Multaconci> updateMultaconci(@Valid @RequestBody Multaconci multaconci) throws URISyntaxException {
        log.debug("REST request to update Multaconci : {}", multaconci);
        if (multaconci.getId() == null) {
            return createMultaconci(multaconci);
        }
        Multaconci result = multaconciRepository.save(multaconci);
        multaconciSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, multaconci.getId().toString()))
            .body(result);
    }

    /**
     * GET  /multaconcis : get all the multaconcis.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of multaconcis in body
     */
    @GetMapping("/multaconcis")
    @Timed
    public List<Multaconci> getAllMultaconcis() {
        log.debug("REST request to get all Multaconcis");
        return multaconciRepository.findAll();
        }

    /**
     * GET  /multaconcis/:id : get the "id" multaconci.
     *
     * @param id the id of the multaconci to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the multaconci, or with status 404 (Not Found)
     */
    @GetMapping("/multaconcis/{id}")
    @Timed
    public ResponseEntity<Multaconci> getMultaconci(@PathVariable Long id) {
        log.debug("REST request to get Multaconci : {}", id);
        Multaconci multaconci = multaconciRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(multaconci));
    }

    /**
     * DELETE  /multaconcis/:id : delete the "id" multaconci.
     *
     * @param id the id of the multaconci to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/multaconcis/{id}")
    @Timed
    public ResponseEntity<Void> deleteMultaconci(@PathVariable Long id) {
        log.debug("REST request to delete Multaconci : {}", id);
        multaconciRepository.delete(id);
        multaconciSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/multaconcis?query=:query : search for the multaconci corresponding
     * to the query.
     *
     * @param query the query of the multaconci search
     * @return the result of the search
     */
    @GetMapping("/_search/multaconcis")
    @Timed
    public List<Multaconci> searchMultaconcis(@RequestParam String query) {
        log.debug("REST request to search Multaconcis for query {}", query);
        return StreamSupport
            .stream(multaconciSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
