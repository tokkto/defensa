package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Estexpedien;
import pe.gob.trabajo.repository.EstexpedienRepository;
import pe.gob.trabajo.repository.search.EstexpedienSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EstexpedienResource REST controller.
 *
 * @see EstexpedienResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class EstexpedienResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private EstexpedienRepository estexpedienRepository;

    @Autowired
    private EstexpedienSearchRepository estexpedienSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEstexpedienMockMvc;

    private Estexpedien estexpedien;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EstexpedienResource estexpedienResource = new EstexpedienResource(estexpedienRepository, estexpedienSearchRepository);
        this.restEstexpedienMockMvc = MockMvcBuilders.standaloneSetup(estexpedienResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Estexpedien createEntity(EntityManager em) {
        Estexpedien estexpedien = new Estexpedien()
            .vDescrip(DEFAULT_V_DESCRIP)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return estexpedien;
    }

    @Before
    public void initTest() {
        estexpedienSearchRepository.deleteAll();
        estexpedien = createEntity(em);
    }

    @Test
    @Transactional
    public void createEstexpedien() throws Exception {
        int databaseSizeBeforeCreate = estexpedienRepository.findAll().size();

        // Create the Estexpedien
        restEstexpedienMockMvc.perform(post("/api/estexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estexpedien)))
            .andExpect(status().isCreated());

        // Validate the Estexpedien in the database
        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeCreate + 1);
        Estexpedien testEstexpedien = estexpedienList.get(estexpedienList.size() - 1);
        assertThat(testEstexpedien.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testEstexpedien.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testEstexpedien.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testEstexpedien.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testEstexpedien.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testEstexpedien.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testEstexpedien.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testEstexpedien.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Estexpedien in Elasticsearch
        Estexpedien estexpedienEs = estexpedienSearchRepository.findOne(testEstexpedien.getId());
        assertThat(estexpedienEs).isEqualToComparingFieldByField(testEstexpedien);
    }

    @Test
    @Transactional
    public void createEstexpedienWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = estexpedienRepository.findAll().size();

        // Create the Estexpedien with an existing ID
        estexpedien.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEstexpedienMockMvc.perform(post("/api/estexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estexpedien)))
            .andExpect(status().isBadRequest());

        // Validate the Estexpedien in the database
        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = estexpedienRepository.findAll().size();
        // set the field null
        estexpedien.setvDescrip(null);

        // Create the Estexpedien, which fails.

        restEstexpedienMockMvc.perform(post("/api/estexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estexpedien)))
            .andExpect(status().isBadRequest());

        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = estexpedienRepository.findAll().size();
        // set the field null
        estexpedien.setnUsuareg(null);

        // Create the Estexpedien, which fails.

        restEstexpedienMockMvc.perform(post("/api/estexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estexpedien)))
            .andExpect(status().isBadRequest());

        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = estexpedienRepository.findAll().size();
        // set the field null
        estexpedien.settFecreg(null);

        // Create the Estexpedien, which fails.

        restEstexpedienMockMvc.perform(post("/api/estexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estexpedien)))
            .andExpect(status().isBadRequest());

        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = estexpedienRepository.findAll().size();
        // set the field null
        estexpedien.setnFlgactivo(null);

        // Create the Estexpedien, which fails.

        restEstexpedienMockMvc.perform(post("/api/estexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estexpedien)))
            .andExpect(status().isBadRequest());

        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = estexpedienRepository.findAll().size();
        // set the field null
        estexpedien.setnSedereg(null);

        // Create the Estexpedien, which fails.

        restEstexpedienMockMvc.perform(post("/api/estexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estexpedien)))
            .andExpect(status().isBadRequest());

        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEstexpediens() throws Exception {
        // Initialize the database
        estexpedienRepository.saveAndFlush(estexpedien);

        // Get all the estexpedienList
        restEstexpedienMockMvc.perform(get("/api/estexpediens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(estexpedien.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getEstexpedien() throws Exception {
        // Initialize the database
        estexpedienRepository.saveAndFlush(estexpedien);

        // Get the estexpedien
        restEstexpedienMockMvc.perform(get("/api/estexpediens/{id}", estexpedien.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(estexpedien.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingEstexpedien() throws Exception {
        // Get the estexpedien
        restEstexpedienMockMvc.perform(get("/api/estexpediens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEstexpedien() throws Exception {
        // Initialize the database
        estexpedienRepository.saveAndFlush(estexpedien);
        estexpedienSearchRepository.save(estexpedien);
        int databaseSizeBeforeUpdate = estexpedienRepository.findAll().size();

        // Update the estexpedien
        Estexpedien updatedEstexpedien = estexpedienRepository.findOne(estexpedien.getId());
        updatedEstexpedien
            .vDescrip(UPDATED_V_DESCRIP)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restEstexpedienMockMvc.perform(put("/api/estexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEstexpedien)))
            .andExpect(status().isOk());

        // Validate the Estexpedien in the database
        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeUpdate);
        Estexpedien testEstexpedien = estexpedienList.get(estexpedienList.size() - 1);
        assertThat(testEstexpedien.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testEstexpedien.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testEstexpedien.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testEstexpedien.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testEstexpedien.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testEstexpedien.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testEstexpedien.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testEstexpedien.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Estexpedien in Elasticsearch
        Estexpedien estexpedienEs = estexpedienSearchRepository.findOne(testEstexpedien.getId());
        assertThat(estexpedienEs).isEqualToComparingFieldByField(testEstexpedien);
    }

    @Test
    @Transactional
    public void updateNonExistingEstexpedien() throws Exception {
        int databaseSizeBeforeUpdate = estexpedienRepository.findAll().size();

        // Create the Estexpedien

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEstexpedienMockMvc.perform(put("/api/estexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(estexpedien)))
            .andExpect(status().isCreated());

        // Validate the Estexpedien in the database
        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEstexpedien() throws Exception {
        // Initialize the database
        estexpedienRepository.saveAndFlush(estexpedien);
        estexpedienSearchRepository.save(estexpedien);
        int databaseSizeBeforeDelete = estexpedienRepository.findAll().size();

        // Get the estexpedien
        restEstexpedienMockMvc.perform(delete("/api/estexpediens/{id}", estexpedien.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean estexpedienExistsInEs = estexpedienSearchRepository.exists(estexpedien.getId());
        assertThat(estexpedienExistsInEs).isFalse();

        // Validate the database is empty
        List<Estexpedien> estexpedienList = estexpedienRepository.findAll();
        assertThat(estexpedienList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchEstexpedien() throws Exception {
        // Initialize the database
        estexpedienRepository.saveAndFlush(estexpedien);
        estexpedienSearchRepository.save(estexpedien);

        // Search the estexpedien
        restEstexpedienMockMvc.perform(get("/api/_search/estexpediens?query=id:" + estexpedien.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(estexpedien.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Estexpedien.class);
        Estexpedien estexpedien1 = new Estexpedien();
        estexpedien1.setId(1L);
        Estexpedien estexpedien2 = new Estexpedien();
        estexpedien2.setId(estexpedien1.getId());
        assertThat(estexpedien1).isEqualTo(estexpedien2);
        estexpedien2.setId(2L);
        assertThat(estexpedien1).isNotEqualTo(estexpedien2);
        estexpedien1.setId(null);
        assertThat(estexpedien1).isNotEqualTo(estexpedien2);
    }
}
